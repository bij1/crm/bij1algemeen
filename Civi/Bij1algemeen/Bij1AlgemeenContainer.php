<?php
/**
 * @author Erik Hommel <erik.hommel@civicoop.org>
 * @date 13 Oct 2021
 * @license AGPL-3.0
 */
namespace Civi\Bij1algemeen;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use CRM_Bij1algemeen_ExtensionUtil as E;

class Bij1AlgemeenContainer implements CompilerPassInterface {

  /**
   * You can modify the container here before it is dumped to PHP code.
   */
  public function process(ContainerBuilder $container) {
    $definition = new Definition('CRM_Bij1algemeen_Bij1AlgemeenService');
    $definition->setFactory(['CRM_Bij1algemeen_Bij1AlgemeenService', 'getInstance']);
    $definition->addMethodCall('setMollieCustomerIdIdentityType', ["mollie_customer_id"]);
    $this->setDefaultLocationType($definition);
    $this->setGroups($definition);
    $this->setBankAccountReference($definition);
    $this->setCustomData($definition);
    $definition->setPublic(TRUE);
    $container->setDefinition('bij1Algemeen', $definition);
  }

  /**
   * Method om custom data in te stellen
   * @param $definition
   */
  private function setCustomData(&$definition) {
    $cgQuery = "SELECT id, name, table_name FROM civicrm_custom_group WHERE extends = %1 AND name IN (%2, %3)";
    $customGroup = \CRM_Core_DAO::executeQuery($cgQuery, [
      1 => ["Contact", "String"],
      2 => ["airtable_data", "String"],
      3 => ["mollie_betaling_data", "String"],
    ]);
    while ($customGroup->fetch()) {
      switch ($customGroup->name) {
        case "airtable_data":
          $definition->addMethodCall('setAirtableDataCustomGroupId', [(int) $customGroup->id]);
          $definition->addMethodCall('setAirtableDataTableName', [$customGroup->table_name]);
          $cfQuery = "SELECT id, column_name FROM civicrm_custom_field WHERE custom_group_id = %1 AND name IN (%2, %3)";
          $customField = \CRM_Core_DAO::executeQuery($cfQuery, [
            1 => [(int) $customGroup->id, "Integer"],
            2 => ["lidnummer", "String"],
            3 => ["inschrijvingsdatum", "String"],
          ]);
          while ($customField->fetch()) {
            switch ($customField->name) {
              case "inschrijvingsdatum":
                $definition->addMethodCall('setInschrijvingsdatumCustomFieldId', [(int) $customField->id]);
                $definition->addMethodCall('setInschrijvingsdatumColumnName', [$customField->column_name]);
                break;
              case "lidnummer":
                $definition->addMethodCall('setLidnummerCustomFieldId', [(int) $customField->id]);
                $definition->addMethodCall('setLidnummerColumnName', [$customField->column_name]);
                break;
            }
          }
          break;
        case "mollie_betaling_data":
          $definition->addMethodCall('setMollieBetalingDataCustomGroupId', [(int) $customGroup->id]);
          $definition->addMethodCall('setMollieBetalingDataTableName', [$customGroup->table_name]);
          $cfQuery = "SELECT id, column_name FROM civicrm_custom_field WHERE custom_group_id = %1 AND name IN (%2, %3)";
          $customField = \CRM_Core_DAO::executeQuery($cfQuery, [
            1 => [(int) $customGroup->id, "Integer"],
            2 => ["klantnaam", "String"],
            3 => ["klantemail", "String"],
          ]);
          while ($customField->fetch()) {
            switch ($customField->name) {
              case "klantnaam":
                $definition->addMethodCall('setKlantnaamCustomFieldId', [(int) $customField->id]);
                $definition->addMethodCall('setKlantnaamColumnName', [$customField->column_name]);
                break;
              case "klantemail":
                $definition->addMethodCall('setKlantemailCustomFieldId', [(int) $customField->id]);
                $definition->addMethodCall('setKlantemailColumnName', [$customField->column_name]);
                break;
              case "mandaat_id":
                $definition->addMethodCall('setMandaatIdCustomFieldId', [(int) $customField->id]);
                $definition->addMethodCall('setMandaatIdColumnName', [$customField->column_name]);
                break;
            }
          }
      }
    }
  }

  /**
   * Method om iban bank account reference in te stellen
   *
   * @param $definition
   */
  private function setBankAccountReference(&$definition) {
      $query = "SELECT cov.value
          FROM civicrm_option_group AS cog JOIN civicrm_option_value AS cov ON cog.id = cov.option_group_id
          WHERE cog.name = %1 AND cov.name = %2";
      $ibanValue = \CRM_Core_DAO::singleValueQuery($query, [
        1 => ["civicrm_banking.reference_types", "String"],
        2 => ["IBAN", "String"],
      ]);
      if ($ibanValue) {
        $definition->addMethodCall('setIbanAccountReference', [$ibanValue]);
      }
  }

  /**
   * Method om groepsids op te slaan
   *
   * @param $definition
   */
  private function setGroups(&$definition) {
    $query = "SELECT id, name FROM civicrm_group WHERE name IN(%1, %2, %3, %4, %5, %6)";
    $dao = \CRM_Core_DAO::executeQuery($query, [
      1 => ["bij1_leden", "String"],
      2 => ["bij1_betaling_gemist", "String"],
      3 => ["bij1_ex_leden", "String"],
      4 => ["bij1_nieuwe_leden", "String"],
      5 => ["bij1_sociaal_tarief", "String"],
      6 => ["bij1_standaard_leden", "String"],
      ]);
    while ($dao->fetch()) {
      switch ($dao->name) {
        case "bij1_leden":
          $definition->addMethodCall('setLedenGroepId', [(int) $dao->id]);
          break;
        case "bij1_betaling_gemist":
          $definition->addMethodCall('setBetalingGemistLedenGroepId', [(int) $dao->id]);
          break;
        case "bij1_ex_leden":
          $definition->addMethodCall('setExLedenGroepId', [(int) $dao->id]);
          break;
        case "bij1_nieuwe_leden":
          $definition->addMethodCall('setNieuweLedenGroepId', [(int) $dao->id]);
          break;
        case "bij1_sociaal_tarief":
          $definition->addMethodCall('setSociaalTariefLedenGroepId', [(int) $dao->id]);
          break;
        case "bij1_standaard_leden":
          $definition->addMethodCall('setStandaardLedenGroepId', [(int) $dao->id]);
          break;
      }
    }
  }

  /**
   * Zet properties voor standaard locatie type
   *
   * @param $definition
   */
  private function setDefaultLocationType(&$definition) {
    $query = "SELECT id FROM civicrm_location_type WHERE is_default = TRUE;";
    $id = \CRM_Core_DAO::singleValueQuery($query);
    if ($id) {
      $definition->addMethodCall('setDefaultLocationTypeId', [(int) $id]);
    }
  }


}

