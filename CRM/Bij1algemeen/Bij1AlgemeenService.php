<?php
use CRM_Bij1algemeen_ExtensionUtil as E;

/**
 * Class BIJ1 Algemeen service
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @date 9 Dec 2021
 * @license AGPL-3.0
 */
class CRM_Bij1algemeen_Bij1AlgemeenService {
  /**
   * @var CRM_Bij1algemeen_Bij1AlgemeenService
   */
  protected static $singleton;
  private $_defaultLocationTypeId = NULL;
  private $_ibanAccountReference = NULL;
  private $_ledenGroepId = NULL;
  private $_betalingGemistLedenGroepId = NULL;
  private $_exLedenGroepId = NULL;
  private $_nieuweLedenGroepId = NULL;
  private $_sociaalTariefLedenGroepId = NULL;
  private $_standaardLedenGroepId = NULL;
  private $_airtableDataCustomGroupId = NULL;
  private $_airtableDataTableName = NULL;
  private $_mollieBetalingDataCustomGroupId = NULL;
  private $_mollieBetalingDataTableName = NULL;
  private $_inschrijvingsdatumCustomFieldId = NULL;
  private $_inschrijvingsdatumColumnName = NULL;
  private $_lidnummerCustomFieldId = NULL;
  private $_lidnummerColumnName = NULL;
  private $_klantnaamCustomFieldId = NULL;
  private $_klantnaamColumnName = NULL;
  private $_klantemailCustomFieldId = NULL;
  private $_klantemailColumnName = NULL;
  private $_mandaatIdCustomFieldId = NULL;
  private $_mandaatIdColumnName = NULL;
  private $_mollieCustomerIdIdentityType = NULL;

  /**
   * CRM_Bij1algemeen_Bij1AlgemeenService constructor.
   */
  public function __construct() {
    if (!self::$singleton) {
      self::$singleton = $this;
    }
  }

  /**
   * @return \CRM_Bij1algemeen_Bij1AlgemeenService
   */
  public static function getInstance() {
    if (!self::$singleton) {
      self::$singleton = new CRM_Bij1algemeen_Bij1AlgemeenService();
    }
    return self::$singleton;
  }

  /**
   * @param int $id
   */
  public function setBetalingGemistLedenGroepId(int $id) {
    $this->_betalingGemistLedenGroepId = $id;
  }

  /**
   * @return null
   */
  public function getBetalingGemistLedenGroepId() {
    return $this->_betalingGemistLedenGroepId;
  }

  /**
   * @param int $id
   */
  public function setExLedenGroepId(int $id) {
    $this->_exLedenGroepId = $id;
  }

  /**
   * @return null
   */
  public function getExLedenGroepId() {
    return $this->_exLedenGroepId;
  }

  /**
   * @param int $id
   */
  public function setLedenGroepId(int $id) {
    $this->_ledenGroepId = $id;
  }

  /**
   * @return null
   */
  public function getLedenGroepId() {
    return $this->_ledenGroepId;
  }

  /**
   * @param int $id
   */
  public function setNieuweLedenGroepId(int $id) {
    $this->_nieuweLedenGroepId = $id;
  }

  /**
   * @return null
   */
  public function getNieuweLedenGroepId() {
    return $this->_nieuweLedenGroepId;
  }

  /**
   * @param int $id
   */
  public function setSociaalTariefLedenGroepId(int $id) {
    $this->_sociaalTariefLedenGroepId = $id;
  }

  /**
   * @return null
   */
  public function getSociaalTariefLedenGroepId() {
    return $this->_sociaalTariefLedenGroepId;
  }

  /**
   * @param int $id
   */
  public function setStandaardLedenGroepId(int $id) {
    $this->_standaardLedenGroepId = $id;
  }

  /**
   * @return null
   */
  public function getStandaardLedenGroepId() {
    return $this->_standaardLedenGroepId;
  }

  /**
   * @param int $id
   */
  public function setDefaultLocationTypeId(int $id) {
    $this->_defaultLocationTypeId = $id;
  }

  /**
   * @return null
   */
  public function getDefaultLocationTypeId() {
    return $this->_defaultLocationTypeId;
  }

  /**
   * @param string
   */
  public function setIbanAccountReference(string $value) {
    $this->_ibanAccountReference = $value;
  }

  /**
   * @return null
   */
  public function getIbanAccountReference() {
    return $this->_ibanAccountReference;
  }

  /**
   * @param int
   */
  public function setAirtableDataCustomGroupId(int $id) {
    $this->_airtableDataCustomGroupId = $id;
  }

  /**
   * @return null
   */
  public function getAirtableDataCustomGroupId() {
    return $this->_airtableDataCustomGroupId;
  }

  /**
   * @param string
   */
  public function setAirtableDataTableName(string $name) {
    $this->_airtableDataTableName = $name;
  }

  /**
   * @return null
   */
  public function getAirtableDataTableName() {
    return $this->_airtableDataTableName;
  }

  /**
   * @param int
   */
  public function setMollieBetalingDataCustomGroupId(int $id) {
    $this->_mollieBetalingDataCustomGroupId = $id;
  }

  /**
   * @return null
   */
  public function getMollieBetalingDataCustomGroupId() {
    return $this->_mollieBetalingDataCustomGroupId;
  }

  /**
   * @param string
   */
  public function setMollieBetalingDataTableName(string $name) {
    $this->_mollieBetalingDataTableName = $name;
  }

  /**
   * @return null
   */
  public function getMollieBetalingDataTableName() {
    return $this->_mollieBetalingDataTableName;
  }

  /**
   * @param int
   */
  public function setInschrijvingsdatumCustomFieldId(int $id) {
    $this->_inschrijvingsdatumCustomFieldId = $id;
  }

  /**
   * @return null
   */
  public function getInschrijvingsdatumCustomFieldId() {
    return $this->_inschrijvingsdatumCustomFieldId;
  }

  /**
   * @param string
   */
  public function setInschrijvingsdatumColumnName(string $name) {
    $this->_inschrijvingsdatumColumnName = $name;
  }

  /**
   * @return null
   */
  public function getInschrijvingsdatumColumnName() {
    return $this->_inschrijvingsdatumColumnName;
  }

  /**
   * @param string
   */
  public function setMollieCustomerIdIdentityType(string $name) {
    $this->_mollieCustomerIdIdentityType = $name;
  }

  /**
   * @return null
   */
  public function getMollieCustomerIdIdentityType() {
    return $this->_mollieCustomerIdIdentityType;
  }

  /**
   * @param int
   */
  public function setLidnummerCustomFieldId(int $id) {
    $this->_lidnummerCustomFieldId = $id;
  }

  /**
   * @return null
   */
  public function getLidnummerCustomFieldId() {
    return $this->_lidnummerCustomFieldId;
  }

  /**
   * @param string
   */
  public function setLidnummerColumnName(string $name) {
    $this->_lidnummerColumnName = $name;
  }

  /**
   * @return null
   */
  public function getLidnummerColumnName() {
    return $this->_lidnummerColumnName;
  }

  /**
   * @param int
   */
  public function setKlantnaamCustomFieldId(int $id) {
    $this->_klantnaamCustomFieldId = $id;
  }

  /**
   * @return null
   */
  public function getKlantnaamCustomFieldId() {
    return $this->_klantnaamCustomFieldId;
  }

  /**
   * @param string
   */
  public function setKlantnaamColumnName(string $name) {
    $this->_klantnaamColumnName = $name;
  }

  /**
   * @return null
   */
  public function getKlantnaamColumnName() {
    return $this->_klantnaamColumnName;
  }

  /**
   * @param int
   */
  public function setKlantemailCustomFieldId(int $id) {
    $this->_klantemailCustomFieldId = $id;
  }

  /**
   * @return null
   */
  public function getKlantemailCustomFieldId() {
    return $this->_klantemailCustomFieldId;
  }

  /**
   * @param string
   */
  public function setKlantemailColumnName(string $name) {
    $this->_klantemailColumnName = $name;
  }

  /**
   * @return null
   */
  public function getKlantemailColumnName() {
    return $this->_klantemailColumnName;
  }

  /**
   * @param int
   */
  public function setMandaatIdCustomFieldId(int $id) {
    $this->_mandaatIdCustomFieldId = $id;
  }

  /**
   * @return null
   */
  public function getMandaatIdCustomFieldId() {
    return $this->_mandaatIdCustomFieldId;
  }

  /**
   * @param string
   */
  public function setMandaatIdColumnName(string $name) {
    $this->_mandaatIdColumnName = $name;
  }

  /**
   * @return null
   */
  public function getMandaatIdColumnName() {
    return $this->_mandaatIdColumnName;
  }

  /**
   * Method om dao in array te stoppen en de 'overbodige' data er uit te slopen
   *
   * @param  $dao
   * @return array
   */
  public function moveDaoToArray($dao) {
    $ignores = array('N', 'id', 'entity_id');
    $columns = get_object_vars($dao);
    // first remove all columns starting with _
    foreach ($columns as $key => $value) {
      if (substr($key, 0, 1) == '_') {
        unset($columns[$key]);
      }
      if (in_array($key, $ignores)) {
        unset($columns[$key]);
      }
    }
    return $columns;
  }

}
