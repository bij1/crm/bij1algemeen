<?php
use CRM_Bij1algemeen_ExtensionUtil as E;

/**
 * Class om Contact Identity verwerking te doen
 * - zet de custom group en option group naar reserved
 * - voeg identity tracker type voor mollie customer id toe
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @license AGPL-3.0
 */
class CRM_Bij1algemeen_IdentityTracker {

  /**
   * Method om de contact identity tracker custom en option group op reserved te zetten
   */
  public function setIsReserved() {
    CRM_Core_DAO::executeQuery("UPDATE civicrm_custom_group SET is_reserved = TRUE WHERE name = %1", [1 => ["contact_id_history", "String"]]);
    CRM_Core_DAO::executeQuery("UPDATE civicrm_option_group SET is_reserved = TRUE WHERE name = %1", [1 => ["contact_id_history_type", "String"]]);
  }

  /**
   * Maak identity tracker type voor mollie customer ID aan
   */
  public function maakMollieCustomerIdType() {
    try {
      $optionValues = \Civi\Api4\OptionValue::get()
        ->addSelect('COUNT(*) AS count')
        ->addWhere('option_group_id:name', '=', 'contact_id_history_type')
        ->addWhere('name', '=', 'mollie_customer_id')
        ->execute();
      $optionValue = $optionValues->first();
      if ($optionValue['count'] == 0) {
        \Civi\Api4\OptionValue::create()
          ->addValue('option_group_id.name', 'contact_id_history_type')
          ->addValue('label', 'Mollie Customer ID')
          ->addValue('value', 'mollie_customer_id')
          ->addValue('name', 'mollie_customer_id')
          ->addValue('description', 'contact identity type voor Mollie Customer ID')
          ->addValue('is_active', TRUE)
          ->addValue('is_reserved', TRUE)
          ->execute();
      }
    }
    catch (API_Exception $ex) {
      Civi::log()->error("Kon geen identity history type voor Mollie Customer ID aanmaken, foutmelding van API4 OptionValue create: " . $ex->getMessage());
    }
  }
}
