<?php
use CRM_Bij1algemeen_ExtensionUtil as E;

/**
 * Class om custom data aan te maken als die nog niet bestaat
 * - veld customer_id in custom group bij1_contact_data voor het Mollie customer id
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @license AGPL-3.0
 */
class CRM_Bij1algemeen_CustomData {

  /**
   * Set the custom data for airtable data
   *
   * @return array[]
   */
  public function getAirtableData() {
    return [
      'airtable_data' => [
        'title' => "Airtable Data",
        'extends' => "Contact",
        'style' => "Tab",
        'table_name' => "civicrm_value_airtable_data",
        'is_multiple' => FALSE,
        'custom_field' => [
          [
          'name' => "lidnummer",
          'label' => "Lidnummer",
          'data_type' => "String",
          'html_type' => "Text",
          'text_length' => "32",
          ],
          [
          'name' => "inschrijvingsdatum",
          'label' => "Inschrijvingsdatum",
          'data_type' => "Date",
          'html_type' => "Select Date",
          'start_date_years' => "5",
          'end_date_years' => "1",
          'date_format' => 'dd-mm-yy'
          ],
        ],
      ],
    ];
  }

  /**
   * Set the custom data for mollie betaling data
   *
   * @return array[]
   */
  public function getMollieBetalingData() {
    return [
      'mollie_betaling_data' => [
        'title' => "Mollie Betaling Data",
        'extends' => "Contribution",
        'style' => "Inline",
        'table_name' => "civicrm_value_mollie_betaling_data",
        'is_multiple' => FALSE,
        'custom_field' => [
          [
            'name' => "mandaat_id",
            'label' => "mandateId",
            'data_type' => "String",
            'html_type' => "Text",
            'text_length' => "64",
          ],
          [
            'name' => "klantnaam",
            'label' => "customer.name",
            'data_type' => "String",
            'html_type' => "Text",
            'text_length' => "64",
          ],
          [
            'name' => "klantemail",
            'label' => "customer.email",
            'data_type' => "String",
            'html_type' => "Text",
            'text_length' => "255",
          ],
        ],
      ],
    ];
  }

  /**
   * Method om te kijken of de custom group al bestaat
   *
   * @param string $customGroupName
   * @return bool
   */
  public function bestaatCustomGroup(string $customGroupName) {
    $query = "SELECT COUNT(*) FROM civicrm_custom_group WHERE name = %1";
    $count = CRM_Core_DAO::singleValueQuery($query, [1 => [$customGroupName, "String"]]);
    if ($count > 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Method om custom data toe te voegen
   *
   * @param array $customData
   */
  public function maak(array $customData) {
    foreach ($customData as $customGroupName => $customGroupData) {
      if (!$this->bestaatCustomGroup($customGroupName)) {
        try {
          $customGroups = \Civi\Api4\CustomGroup::create()
            ->addValue('name', $customGroupName)
            ->addValue('title', $customGroupData['title'])
            ->addValue('extends', $customGroupData['extends'])
            ->addValue('style', $customGroupData['style'])
            ->addValue('is_multiple', $customGroupData['is_multiple'])
            ->addValue('is_active', TRUE)
            ->addValue('is_public', TRUE)
            ->addValue('is_reserved', TRUE)
            ->addValue('collapse_display', FALSE)
            ->addValue('table_name', $customGroupData['table_name']);
          if (isset($customGroupData['extends_entity_column_value'])) {
            $customGroups->addValue('extends_entity_column_value', $customGroupData['extends_entity_column_value']);
          }
          $customGroups->execute();
          $customGroupId = $this->haalCustomGroupId($customGroupName);
          if ($customGroupId) {
            foreach ($customGroupData['custom_field'] as $customFieldData) {
              $customField = \Civi\Api4\CustomField::create()
                ->addValue('custom_group_id', $customGroupId)
                ->addValue('name', $customFieldData['name'])
                ->addValue('column_name', $customFieldData['name'])
                ->addValue('label', $customFieldData['label'])
                ->addValue('data_type', $customFieldData['data_type'])
                ->addValue('html_type', $customFieldData['html_type'])
                ->addValue('is_searchable', TRUE)
                ->addValue('is_view', TRUE)
                ->addValue('is_active', TRUE)
                ->addValue('in_selector', TRUE)
                ->addValue('is_reserved', TRUE);
              if (isset($customFieldData['text_length'])) {
                $customField->addValue('text_length', $customFieldData['text_length']);
              }
              if (isset($customFieldData['date_format'])) {
                $customField->addValue('date_format', $customFieldData['date_format']);
              }
              if (isset($customFieldData['start_date_years'])) {
                $customField->addValue('start_date_years', $customFieldData['start_date_years']);
              }
              if (isset($customFieldData['end_date_years'])) {
                $customField->addValue('end_date_years', $customFieldData['end_date_years']);
              }
              $customField->execute();
            }
          }
        }
        catch (API_Exception $ex) {
          Civi::log()->error(E::ts("Fout bij aanmaken custom data met API4 error: ") . $ex->getMessage());
        }
      }
    }
  }

  /**
   * Method om custom group id op te halen
   *
   * @param string $customGroupName
   * @return false|int
   */
  private function haalCustomGroupId(string $customGroupName) {
    if (!empty($customGroupName)) {
      $query = "SELECT id FROM civicrm_custom_group WHERE name = %1 LIMIT 1";
      $customGroupId = CRM_Core_DAO::singleValueQuery($query, [1 => [$customGroupName, "String"]]);
      if ($customGroupId) {
        return (int) $customGroupId;
      }
    }
    return FALSE;
  }

}
