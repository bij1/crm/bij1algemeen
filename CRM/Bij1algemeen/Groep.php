<?php
use CRM_Bij1migratie_ExtensionUtil as E;

/**
 * Groepen aanmaken als ze nog niet bestaan
 *
 * @author Erik Hommel(CiviCooP) <erik.hommel@civicoop.org>
 * @license AGPL-3.0
 */
class CRM_Bij1algemeen_Groep {

  /**
   * Method om groep aan te maken
   * @param array $groepsData
   */
  public function maakGroep(array $groepsData) {
    if (!empty($groepsData) && isset($groepsData['name'])) {
      if (!isset($groepsData['title'])) {
        $delen = explode("_", $groepsData['name']);
        foreach ($delen as $deelKey => $deel) {
          $delen[$deelKey] = ucfirst(strtolower($deel));
        }
        $groepsData['title'] = implode(" ", $delen);
      }
      try {
        $groep = \Civi\Api4\Group::create()
          ->addValue('name', $groepsData['name'])
          ->addValue('title', $groepsData['title'])
          ->addValue('is_active', TRUE)
          ->addValue('group_type', [2])
          ->addValue('is_reserved', TRUE);
        if (isset($groepsData['description'])) {
          $groep->addValue('description', $groepsData['description']);
        }
        if (isset($groepsData['parents:name'])) {
          $groep->addValue('parents:name', [$groepsData['parents:name']]);
        }
        $groep->execute();
      }
      catch (API_Exception $ex) {
        Civi::log()->error("Kon geen groep aanmaken met data: " . json_encode($groepsData) . ", foutboodschap van API4 Group create: " . $ex->getMessage());
      }
    }
  }

  /**
   * Method om te checken of de groep bestaat
   *
   * @param string $groepsNaam
   * @return bool
   */
  public function bestaatGroep(string $groepsNaam) {
    if (!empty($groepsNaam)) {
      try {
        $groups = \Civi\Api4\Group::get()
          ->addSelect('COUNT(*) AS count')
          ->addWhere('name', '=', $groepsNaam)
          ->execute();
        foreach ($groups as $group) {
          if ($group['count'] > 0) {
            return TRUE;
          }
        }
      }
      catch (API_Exception $ex) {
        Civi::log()->error("Kon geen groep zoeken met naam " . $groepsNaam . " in " . __METHOD__ . ", foutboodschap van API4 Group get: " . $ex->getMessage());
      }
    }
    return FALSE;
  }

  /**
   * Method om leden groepen op te halen
   *
   * @return string[][]
   */
  private function haalLedenGroepen() {
    return [
      [
        'name' => "bij1_leden",
        'title' => "Alle leden",
        'description' => "Deze groep bevat alle leden van Bij1 en heeft verschillende subgroepen",
      ],
      [
        'name' => "bij1_betaling_gemist",
        'title' => "Betaling gemist",
        'description' => "Deze groep bevat alle leden die een betaling gemist hebben",
      ],
      [
        'name' => "bij1_ex_leden",
        'title' => "Ex-leden",
        'description' => "Deze groep bevat alle ex-leden",
      ],
      [
        'name' => "bij1_nieuwe_leden",
        'title' => "Nieuwe leden",
        'description' => "Deze groep bevat alle nieuwe leden",
      ],
      [
        'name' => "bij1_sociaal_tarief",
        'title' => "Sociaal tarief leden",
        'description' => "Deze groep bevat alle sociaal tarief leden",
      ],
      [
        'name' => "bij1_standaard_leden",
        'title' => "Standaard leden",
        'description' => "Deze groep bevat alle standaard leden",
      ],
    ];
  }

  /**
   * Functie om initiele ledengroepen aan te maken
   */
  public function maakLedenGroepen() {
    $ledenGroepen = $this->haalLedenGroepen();
    foreach ($ledenGroepen as $groepsData) {
      if (!$this->bestaatGroep($groepsData['name'])) {
        $this->maakGroep($groepsData);
      }
    }
    // fix probleem dat ouder groep niet ingevuld wordt
    $hoofdGroepId = $this->haalGroepsIdMetNaam('bij1_leden');
    if ($hoofdGroepId) {
      $kindGroepen = [];
      foreach ($ledenGroepen as $groepsData) {
        if ($groepsData['name'] != "bij1_leden") {
          $kindGroepId = $this->haalGroepsIdMetNaam($groepsData['name']);
          if ($kindGroepId) {
            CRM_Core_DAO::executeQuery("UPDATE civicrm_group SET parents = %1 WHERE id = %2", [
              1 => [$hoofdGroepId, "String"],
              2 => [$kindGroepId, "Integer"],
            ]);
            CRM_Core_DAO::executeQuery("INSERT INTO civicrm_group_nesting (child_group_id, parent_group_id) VALUES(%2, %1)", [
              1 => [$hoofdGroepId, "String"],
              2 => [$kindGroepId, "Integer"],
            ]);
            $kindGroepen[] = $kindGroepId;
          }
        }
      }
      CRM_Core_DAO::executeQuery("UPDATE civicrm_group SET children = %1 WHERE id = %2", [
        1 => [implode(",", $kindGroepen), "String"],
        2 => [$hoofdGroepId, "Integer"],
      ]);
    }
  }

  /**
   * Method om group_id op te halen met name
   *
   * @param string $groepsNaam
   * @return false|int
   */
  public function haalGroepsIdMetNaam(string $groepsNaam) {
    try {
      $groups = \Civi\Api4\Group::get()
        ->addSelect('id')
        ->addWhere('name', '=', $groepsNaam)
        ->execute();
      $group = $groups->first();
      if ($group['id']) {
        return (int) $group['id'];
      }
    }
    catch (API_Exception $ex) {
      Civi::log()->error(E::ts("Kon geen groepsid vinden met groepsnaam: ") . $groepsNaam . E::ts(", foutmelding van API4 Group get: ") . $ex->getMessage());
    }
    return FALSE;
  }

}
